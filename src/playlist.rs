use crate::song::SongName;

const MY_PLAYLISTS_URL: &'static str = "https://api.spotify.com/v1/me/playlists"
const PLAYLISTS_URL: &'static str = "https://api.spotify.com/v1/playlists"

/// The ID of a playlist (https://api.spotify.com/v1/playlists)
struct PlaylistID(String);

struct PlaylistMetadata {
    name: String,
    owner: String,
}

struct Playlist;
impl Playlist {
    fn get_all_playlists() -> Vec<PlaylistID>;

    fn get_playlist_songs(id: PlaylistID) -> Vec<SongName>;

    fn get_playlist_metadata(id: PlaylistID) -> PlaylistMetadata;

}
