use clap::Parser;
use url::Url;

#[derive(Debug, Parser)]
#[clap(author, version, about)]
struct Cli {
    #[clap(flatten)]
    credentials: Option<Credentials>,
    /// Download all the liked songs
    #[clap(long)]
    liked_songs: bool,
    /// Download from the specified playlist
    #[clap(long)]
    playlist: Option<Url>,
}

#[derive(Debug, Parser)]
struct Credentials {
    /// Account email or username
    #[clap(long)]
    username: String,
    /// Account password
    #[clap(long)]
    password: String,
}

fn main() {
    let args = Cli::parse();
    println!("{:#?}", args);
}
